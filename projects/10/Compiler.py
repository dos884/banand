import os
import difflib
from os import listdir

import sys

SEPARATOR = "/"
SHOULD_OUT_XML = True
SHOULD_OUT_VM = False
READABLE_MODE = False
TAB_IS = "    "
__DEBUG__=False


current_vm_code=""
glob_xml=""

Types_KWRD = {
    'class': "class",
    'constructor': "constructor",
    'function': "function",
    'method': "method",
    'field': "field",
    'static': "static",
    'var': "var",
    'int': "int",
    'char': "char",
    'boolean': "boolean",
    'void': "void",
    'true': "true",
    'false': "false",
    'null': "null",
    'this': "this",
    'let': "let",
    'do': "do",
    'if': "if",
    'else': "else",
    'while': "while",
    'return': "return"
}

Types_SYM = {
    '{': '{',
    '}': '}',
    '(': '(',
    ')': ')',
    '[': '[',
    ']': ']',
    '.': '.',
    ',': ',',
    ';': ';',
    '+': '+',
    '-': '-',
    '*': '*',
    '/': '/',
    '&': '&amp;',
    '|': '|',
    '<': '&lt;',
    '>': '&gt;',
    ':': ':',
    '=': '=',
    '~': '~'
}

KWRD_CONSTS = {
    'true': 'true',
    'false': 'false',
    'null': 'null',
    'this': 'this'
}

STRINGSYM = "\""

Log=""


def is_blank(x):
    if (x == "") or (x[:2] == "//") or (x.isspace()):
        return True
    return False


def clear_block_comment(txt):
    while (True):
        marker = txt.find("/*")
        if marker == -1:
            break
        marker2 = txt.find("*/")
        if marker2 == -1:
            raise Exception("non closing comment block recieved!")
        txt = txt[0:marker] + txt[marker2 + 2:]
    return txt


def clear_normal_comment(txt):
    while (True):
        marker = txt.find("//")
        if marker == -1:
            break
        marker2 = txt.find("\n", marker)
        if marker2 == -1:
            raise Exception("non closing comment block recieved!")
        txt = txt[0:marker] + txt[marker2:]
    return txt


def clear_comments(line):
    # take off comment
    marker = line.find("//")
    if marker != -1:
        return line[0:marker]
    return line


def DBG_print(msg):
    # global Log
    # Log+=str(msg)+'\n'
    # print(msg)
    pass


def DBG_print_p11(msg):

    if __DEBUG__:
        # global Log
        # Log+=str(msg)+'\n'
        print(msg)


def DBG_ERROR(str):
    global Log
    # print(current_vm_code)
    raise Exception(str+", printing log:\n"+Log)
    # raise Exception(current_vm_code)


def custom_split(txt):
    import re
    txt_status = txt
    A = []
    length = len(txt)
    marker = 0
    marker_slave = 0
    while (marker + 1 < length):
        while ((marker + 1 < length) and (txt[marker] != "\"")):
            if (txt[marker:marker + 2] == "//") or (txt[marker:marker + 2] == "/*"):
                break
            marker += 1
        # add a split of the selected chunk to the container list
        A = A + [list(filter(None, re.split(r"(;|\{|\}|\(|\)|\[|\]|\.|,|=|\+|-|\*|/|&|\||<|>|~)", s))) for s in
                 txt[marker_slave:marker].split()]
        # A=A+re.split(r"(\s|;|\{|\}|\(|\)|\[|\]|\.|,|\+|-|\*|/|&|\||<|>|~)", txt[marker_slave:marker])
        # now slave is free
        marker_slave = marker
        if not marker + 1 == length:
            if (txt[marker] == "\""):
                marker_slave = txt.find("\"", marker + 1)
                if marker_slave == -1:
                    DBG_ERROR("non closing string block recieved!")
                # At this point we dont want to remove the quotes yet so the
                # tokenizer can recognize the type of the token
                A.append(txt[marker:marker_slave + 1])
                # advance the markers after the selected STRING
                marker_slave += 1
                marker = marker_slave
            if (txt[marker:marker + 2] == "//"):
                marker_slave = txt.find("\n", marker)
                if marker_slave == -1:
                    # we break. no more new lines!
                    break
                marker_slave += 1
                marker = marker_slave
            if (txt[marker:marker + 2] == "/*"):
                marker_slave = txt.find("*/", marker)
                if marker_slave == -1:
                    # we break. no more new lines!
                    DBG_ERROR("non closing block comment")
                marker_slave += 2
                marker = marker_slave
        txt_status = txt[marker:]
    # now flatten the list
    B = []
    for el in A:
        if type(el) == str:
            B.append(el)
        else:
            for inner in el:
                B.append(inner)
    return B

_unique=-1
def unique_suffix():
    global _unique
    _unique+=1
    return str(_unique)

class TypeOfTerminal:
    KEYWORD = "keyword"
    SYMBOL = "symbol"
    IDENTIFIER = "identifier"
    INT_CONST = "integerConstant"
    STRING_CONST = "stringConstant"
    KWRD_CONST = "keyword"
    __All__ =["keyword", "symbol", "identifier", "integerConstant", "stringConstant", "keyword"]

class SegmentType:
    __All__ = ["constant", "argument", "local", "static", "this", "that", "pointer", "temp"]
    CONST = "constant"
    ARG = "argument"
    LOCAL = "local"
    STATIC = "static"
    THIS = "this"
    THAT = "that"
    POINTER = "pointer"
    TEMP = "temp"


class OperType:
    __All__ = {"add", "sub", "neg", "eq", "gt", "lt", "and", "or", "not"}
    ADD = "add"
    SUB = "sub"
    NEG = "neg"
    EQ = "eq"
    GT = "gt"
    LT = "lt"
    AND = "and"
    OR = "or"
    NOT = "not"


class Tokenizer:
    # ^_
    def __init__(self, input_file):
        """(
        Opens the input file/stream and gets
        ready to tokenize it. hasMoreTokens() - Boolean Do we have more tokens in the input?
        )"""
        global current_vm_code
        self.current_token = None
        with open(input_file, 'r') as file:
            read_data = file.read()

        current_vm_code=read_data
        # DBG_print_p11(read_data)

        self._file = input_file
        # self.current_command=None
        self.lineIdx = -1
        self.code = custom_split(read_data)

    # def init_with_string(self, read_data):
    #     read_data = clear_block_comment(read_data)
    #     read_data = clear_normal_comment(read_data)
    #
    #     arr = read_data.split("\n")
    #
    #     # remove bad line
    #     striped = [x for x in arr if not is_blank(x)]
    #     # striped = [x.split() for x in striped if not is_blank(x)]
    #
    #     import re
    #     # break the elements to tokens
    #
    #     striped = [list(filter(None, re.split(r"(;|\{|\}|\(|\)|\[|\]|\.|,|\+|-|\*|/|&|\||<|>|~)", s))) for s in striped]
    #     # \"(.+?)\"
    #     DBG_print(striped)
    #     # now flatten the list
    #     striped = [item for sublist in striped for item in sublist]
    #     DBG_print(striped)
    #     A = []
    #     for el in striped:
    #         if (el[0] == "\""):
    #             A.append(el)
    #         else:
    #             A.append(el.split())
    #     DBG_print(A)
    #
    #     # striped = [item for sublist in A for item in sublist if not len(item)==0]
    #     B = []
    #     for el in A:
    #         if type(el) == str:
    #             B.append(el)
    #         else:
    #             for inner in el:
    #                 B.append(inner)
    #     DBG_print(B)
    #     return B

    def hasMoreTokens(self):
        """- Boolean Do we have more tokens in the input?"""
        return (len(self.code) > (self.lineIdx + 1))

    # ^_^
    def advance(self):
        """(
        Gets the next token from the input
        and makes it the current token. This
        method should only be called if
        hasMoreTokens() is true. Initially
        there is no current token.
        )"""
        if (self.hasMoreTokens()):
            self.lineIdx += 1
            self.current_token = self.code[self.lineIdx]
        else:
            DBG_ERROR("NO MORE TOKENS")

    def peek_next(self):
        if (self.hasMoreTokens()):
            return self.code[self.lineIdx + 1]
        else:
            return None

    # ^_^
    def tokenType(self):
        """(
         - KEYWORD, SYMBOL,
        IDENTIFIER, INT_CONST,
        STRING_CONST
        Returns the type of the current token.
        )"""
        token = self.current_token

        # KEYWORD
        if token in Types_KWRD:
            return TypeOfTerminal.KEYWORD
        # SYMBOL
        if token in Types_SYM:
            return TypeOfTerminal.SYMBOL
        # INT
        if token.isdigit():
            return TypeOfTerminal.INT_CONST
        # STRING
        if token.startswith(STRINGSYM) and token.endswith(STRINGSYM):
            return TypeOfTerminal.STRING_CONST
        # keyword_const
        if token in KWRD_CONSTS:
            return TypeOfTerminal.KWRD_CONST
        # IDENTIFIER
        if ~(token[0].isdigit()):
            return TypeOfTerminal.IDENTIFIER
        DBG_ERROR('bad Type')

    # ^_^
    def keyWord(self):
        """(
        - CLASS, METHOD, FUNCTION,
        CONSTRUCTOR, INT,
        BOOLEAN, CHAR, VOID,
        VAR, STATIC, FIELD, LET,
        DO, IF, ELSE, WHILE,
        RETURN, TRUE, FALSE,
        NULL, THIS
        Returns the keyword which is the
        current token. Should be called only
        when tokenType() is KEYWORD .
        )"""
        if self.tokenType() == TypeOfTerminal.KEYWORD:
            return Types_KWRD[self.current_token]
        else:
            DBG_ERROR('not KWD')

    # ^_^
    def symbol(self):
        """(
         - Char Returns the character which is the
        current token. Should be called only
        when tokenType() is SYMBOL .
        )"""
        if self.tokenType() == TypeOfTerminal.SYMBOL:
            return Types_SYM[self.current_token]
        else:
            DBG_ERROR('not SYM')

    # ^_^
    def identifier(self):
        """(
         - String Returns the identifier which is the
        current token. Should be called only
        when tokenType() is IDENTIFIER .
            )"""
        if self.tokenType() == TypeOfTerminal.IDENTIFIER:
            return self.current_token
        else:
            DBG_ERROR('not IDNTFR')

    # ^_^
    def intVal(self):
        """(
         Int Returns the integer value of the
        current token. Should be called only
        when tokenType() is INT_CONST .
        214 Chapter 10
        Routine Arguments Returns Function
        )"""
        if self.tokenType() == TypeOfTerminal.INT_CONST:
            return int(self.current_token)
        else:
            DBG_ERROR('not INT')

    # ^_^
    def stringVal(self):
        """(
         String Returns the string value of the current
        token, without the double quotes.
        Should be called only when
        tokenType() is STRING_CONST
        )"""
        if self.tokenType() == TypeOfTerminal.STRING_CONST:
            return self.current_token.strip('"')
        else:
            DBG_ERROR('not STRING')

    def tokenToXML(self):
        if self.tokenType() == TypeOfTerminal.KEYWORD:
            return "<" + self.tokenType() + "> " + self.keyWord() + " </" + self.tokenType() + ">"
        if self.tokenType() == TypeOfTerminal.SYMBOL:
            return "<" + self.tokenType() + "> " + self.symbol() + " </" + self.tokenType() + ">"
        if self.tokenType() == TypeOfTerminal.IDENTIFIER:
            return "<" + self.tokenType() + "> " + self.identifier() + " </" + self.tokenType() + ">"
        if self.tokenType() == TypeOfTerminal.INT_CONST:
            return "<" + self.tokenType() + "> " + str(self.intVal()) + " </" + self.tokenType() + ">"
        if self.tokenType() == TypeOfTerminal.STRING_CONST:
            return "<" + self.tokenType() + "> " + self.stringVal() + " </" + self.tokenType() + ">"
        DBG_ERROR("bad token type")

    def printAsXML(self):
        code_chunk = '<tokens>\n'
        while self.hasMoreTokens():
            self.advance()
            code_chunk += self.tokenToXML() + '\n'
        code_chunk += '</tokens>\n'
        self.current_token = None
        self.lineIdx = -1
        return code_chunk


class Symb_type:
    __All__=["static","field","argument","local"]
    STATIC = "static"
    FIELD = "field"
    ARG = "argument"
    VAR = "local"


translate={
    Symb_type.FIELD:"this"
}

class SymbolRow:
    def __init__(self, type_sym, kind, count):
        # self.name = name
        self.type_sym = type_sym
        self.kind = kind
        self.count = count

    def __str__(self):
        return self.type_sym + " : " + self.kind + " : " + str(self.count)


THIS = "this"


class SymbolTable:
    def __init__(self):
        """Creates a new empty symbol
        table."""
        self.subrt_sym_list = dict()
        self.class_sym_list = dict()
        # we keep previous subroutines for debug:
        self.prev_subrts_DBG = []
        self.in_sbtr = False
        self.clss_name = ""
        self.sub_name = None
        self.class_count_field = 0
        self.class_count_stat = 0
        self.subrt_count_arg = 0
        self.subrt_count_var = 0

    def __str__(self):
        string = "   class: " + self.clss_name + '\n'
        for x in self.class_sym_list:
            string += x + " | " + str(self.class_sym_list[x]) + '\n'
        if self.in_sbtr:
            string += "  subrt: " + str(self.clss_name) + "." + str(self.sub_name) + '\n'
            for x in self.subrt_sym_list:
                string += x + " | " + str(self.subrt_sym_list[x]) + '\n'

        # for D in self.prev_subrts_DBG:
        #     string += "* * *\n"
        #     for y in D:
        #         string += y + " | " + str(D[y]) + '\n'

        string += " ----------------- \n"
        return string

    def setClss(self, name):
        self.clss_name = name

    def startSubroutine(self, sub_name):
        self.in_sbtr = True
        prev_name = self.sub_name
        self.sub_name = sub_name
        self.subrt_count_arg = 0
        self.subrt_count_var = 0
        self.subrt_sym_list["_NAME_"] = prev_name
        self.prev_subrts_DBG.append(self.subrt_sym_list)
        self.subrt_sym_list = dict()

    def endSubrt(self):
        """
        exits subroutine scope
        """
        self.in_sbtr = False
        self.sub_name = None

    def Define(self, name, type, kind):
        """Defines a new identifier of a
        given name, type, and kind
        and assigns it a running index.
        STATIC and FIELD identifiers
        have a class scope, while ARG
        and VAR identifiers have a
        subroutine scope."""
        if (self.in_sbtr):
            count = 0
            if kind == Symb_type.ARG:
                count = self.subrt_count_arg
                self.subrt_count_arg += 1
            elif kind == Symb_type.VAR:
                count = self.subrt_count_var
                self.subrt_count_var += 1
            else:
                DBG_ERROR("only VAR or ARG")
            if (name not in self.subrt_sym_list):
                self.subrt_sym_list[name] = SymbolRow(type, kind, count)

            else:
                DBG_ERROR("Trying to define existing name")
        else:
            count = 0
            if kind == Symb_type.FIELD:
                count = self.class_count_field
                self.class_count_field += 1
            elif kind == Symb_type.STATIC:
                count = self.class_count_stat
                self.class_count_stat += 1
            else:
                DBG_ERROR("STATIC or FIELD expected!")

            if (name not in self.class_sym_list):
                self.class_sym_list[name] = SymbolRow(type, kind, count)

            else:
                DBG_ERROR("Trying to define existing name")

    def getByName(self, name):
        """ return:  (STATIC, FIELD, ARG, VAR, NONE )
        Returns the kind of the named
        identifier in the current scope.
        If the identifier is unknown in
        the current scope, , returns NONE
        """
        if (self.in_sbtr):
            if name in self.subrt_sym_list:
                return self.subrt_sym_list[name]
            elif name in self.class_sym_list:
                return self.class_sym_list[name]
            else:
                return None
        else:
            if name in self.class_sym_list:
                return self.class_sym_list[name]
            else:
                return None


class VMWriter:
    # ^VMWFUNC^
    def __init__(self, file):
        """
        Creates a new file and prepares
        it for writing.
        """
        self.working_string = ""
        self.indentation=0
        self.VM_TAB="    "

    def getIndent(self):
        if not READABLE_MODE:
            return  ""
            # if self.indentation==0:
            #     return ""
            # else:
            #     return "/*"+self.VM_TAB*(self.indentation-1)+"*/"
        else:
            return self.VM_TAB*self.indentation

    def append(self, str):
        self.working_string += self.getIndent()+ str + '\n'

    def writeComment(self,comm):
        prvindent=self.indentation
        self.indentation=0
        self.working_string+=self.getIndent()+"/*"+comm+"*/\n"
        self.indentation=prvindent
        pass

    # ^VMWFUNC^
    def writePush(self, segment, index):
        """
         Segment ( CONST,
        ARG, LOCAL,
        STATIC, THIS,
        THAT, POINTER,
        TEMP )
        Index (int)

        Writes a VM push command.
        """
        if segment not in SegmentType.__All__:
            DBG_ERROR("bad segment given")
        self.append("push " + str(segment) + " " + str(index))

    # ^VMWFUNC^
    def writePop(self, segment, index):
        """
        :Segment ( CONST, ARG, LOCAL, STATIC, THIS, THAT, POINTER, TEMP )
        :Index (int)

        Writes a VM pop command.
        """
        if segment not in SegmentType.__All__:
            DBG_ERROR("bad segment given")
        self.append("pop " + str(segment) + " " + str(index))

    # ^VMWFUNC^
    def WriteArithmetic(self, command):
        """
        :command ( ADD, SUB, NEG, EQ, GT, LT, AND, OR, NOT )
        Writes a VM arithmetic
        command.
        """
        if command not in OperType.__All__:
            DBG_ERROR("bad segment given")
        self.append(str(command))

    # ^VMWFUNC^
    def WriteLabel(self, label):
        """
         Writes a VM label command.
        """
        self.indentation=1
        self.append("label " + str(label))
        self.indentation=2

    # ^VMWFUNC^
    def WriteGoto(self, label):
        """
        Writes a VM goto command.
        """
        self.append("goto " + str(label))

    # ^VMWFUNC^
    def WriteIf(self, label):
        """
         ...
        """
        self.append("if-goto " + str(label))

    # ^VMWFUNC^
    def writeCall(self, name, nArgs):
        """
         name (String)
        nArgs (int)
        Writes a VM call command.
        """
        self.append("call " + name + " " + str(nArgs))

    # ^VMWFUNC^
    def writeFunction(self, name, nLocs):
        """
        Writes a VM function
        command.
        """
        self.indentation=0
        self.append("function " + name + " " + str(nLocs))
        self.indentation=1

    # ^VMWFUNC^
    def writeReturn(self):
        """
        Writes a VM return
        command.
        """
        self.append("return")
        # self.indentation=0

    # ^VMWFUNC^
    def close(self):
        """
         Closes the output file.
        """
        # DBG_print_p11(self.working_string)
        pass

def compileSymbToSegment(symb):
    # if type(symb)!=SymbolRow:
    #     if not symb in Symb_type.__All__:
    #         DBG_ERROR("Must pass SymbolRow Object Or Symbol, passed: "+str(symb))
    #     elif symb == Symb_type.FIELD:
    #         return SegmentType.THIS
    #     else:
    #         return symb
    if symb.kind==Symb_type.FIELD:
        return SegmentType.THIS
    else:
        return symb.kind

class CompilationEngine:
    def __init__(self, tokenizer):
        # self.out_file = out_file
        self.tkn = tokenizer
        self.working_string = ""
        self.class_name = ""
        self.field_count=0
        self.symb_table = SymbolTable()
        self.indent_level=0
        self.vmw = VMWriter(None)  # todo give file name
        # START WORK:
        self.compileClass()



    def currentTokenType(self):
        return self.tkn.tokenType()

    def verify(self, assertion):
        if __DEBUG__:
            if not assertion is None:
                if assertion in TypeOfTerminal.__All__:
                    if assertion != self.currentTokenType():
                        DBG_ERROR("Token Assert Error")
                        return False

                elif type(assertion) == str:
                    if assertion != self.tkn.current_token:
                        DBG_ERROR("Token Assert Error")
                        return False

        return True

    def compile_string(self, string_long):
        string = string_long[1:-1]
        self.vmw.writePush(SegmentType.CONST, len(string))
        self.vmw.writeCall('String.new', 1)
        for char in string:
            self.vmw.writePush(SegmentType.CONST, ord(char))
            self.vmw.writeCall('String.appendChar', 2)

    def parseCurrentToken(self, verification):
        self.working_string += self.indent_level*TAB_IS + self.tkn.tokenToXML() + '\n'

    def startXMLNode(self, string):

        self.working_string += self.indent_level*TAB_IS +"<"+string+">\n"
        self.indent_level+=1

    def endXMLNode(self,string):

        self.indent_level-=1
        self.working_string += self.indent_level*TAB_IS +"</"+string+">\n"

    def printAsXML(self):
        return self.working_string

    def getVMCode(self):
        return self.vmw.working_string

    def compileClass(self):
        """

        :return:

        """
        DBG_print("@ compileClass")
        # START+
        start_length = len(self.working_string)
        self.startXMLNode("class")

        self.tkn.advance()  # advance to 0
        self.parseCurrentToken(self.verify("class"))  # class
        self.tkn.advance()
        # --< class identifier
        self.symb_table.setClss(self.tkn.current_token)
        self.class_name=self.tkn.current_token
        # self.parseCurrentToken(self.verify(TypeOfTerminal.IDENTIFIER))  # class name
        self.parseCurrentToken(None)  # class name
        # -->
        self.tkn.advance()  # advance to '{'
        self.parseCurrentToken(self.verify("{"))
        # self.tkn.advance()  # advance to first token of vardec or subroutine
        self.tkn.advance()  # advance to first token of vardec or subroutine
        # while (self.tkn.current_token != "function"):
        while (self.tkn.current_token in ["static", "field"]):
            # if self.tkn.current_token == 'field':
            self.field_count+=self.CompileClassVarDec()

        # DBG_print_p11(self.symb_table)
        while (self.tkn.current_token != "}"):
            self.CompileSubroutine()

        self.parseCurrentToken(self.verify("}"))  # }
        self.endXMLNode("class")
        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        DBG_print(self.working_string)
        DBG_print("@END compileClass")

    def CompileSubroutine(self):
        """

        :return:
        """
        DBG_print("@ CompileSubroutine")
        # START+
        start_length = len(self.working_string)

        self.startXMLNode("subroutineDec")
        type_of_func = self.tkn.current_token
        if type_of_func not in ["constructor", "method", "function"]:
            DBG_print_p11("ERROR\n")  # todo exception?
        self.parseCurrentToken(self.verify(None))  # contructor, method, function
        self.tkn.advance()
        self.parseCurrentToken(self.verify(None))  # void, type
        self.tkn.advance()
        # --
        func_name = self.tkn.current_token
        self.symb_table.startSubroutine(func_name)
        if type_of_func == 'method':
            self.symb_table.Define(THIS, self.class_name, Symb_type.ARG)
        # --
        # self.vmw.writeComment("** START "+ type_of_func+" "+func_name+"***")

        self.parseCurrentToken(self.verify(TypeOfTerminal.IDENTIFIER))  # subName
        self.tkn.advance()
        self.parseCurrentToken(self.verify("("))  # (
        self.tkn.advance()  # advance to the parameters
        count = self.compileParameterList()
        self.parseCurrentToken(self.verify(")"))  # )
        self.tkn.advance()

        self.startXMLNode("subroutineBody")
        self.parseCurrentToken(self.verify("{"))  # {
        DBG_print("compile BODY")
        self.tkn.advance()  # advance to first body token
        var_count=0
        while (self.tkn.current_token == "var"):
            var_count+=self.CompileVarDec()

        # --
        self.vmw.writeFunction(self.class_name+"."+func_name,var_count)
        if type_of_func == 'method':
            # by convention ARG:0 holds the pointer to the object
            # so store it in 'this'
            self.vmw.writePush(SegmentType.ARG,0)
            self.vmw.writePop(SegmentType.POINTER,0)
        if type_of_func=='constructor':
            # allocate memory
            self.vmw.writePush(SegmentType.CONST,self.field_count)
            self.vmw.writeCall("Memory.alloc",1)
            # get the addr of the allocated object and store in 'this'
            self.vmw.writePop(SegmentType.POINTER,0)
        # --

        self.compileStatements()
        self.parseCurrentToken(self.verify("}"))  # }

        # --
        # DBG_print_p11(self.symb_table)
        self.symb_table.endSubrt()
        # --

        self.endXMLNode("subroutineBody")
        self.tkn.advance()

        # END-
        self.endXMLNode("subroutineDec")
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        # advance to after function dec
        DBG_print("@END CompileSubroutine")

    def compileParameterList(self):
        """

        :return:
        """
        DBG_print("@ compileParameterList")
        # START+
        start_length = len(self.working_string)

        self.startXMLNode("parameterList")

        count=0
        while (self.tkn.current_token != ")"):
            type = self.tkn.current_token
            self.parseCurrentToken(self.verify(None))  # type
            self.tkn.advance()
            name = self.tkn.current_token
            self.parseCurrentToken(self.verify(None))  # name
            self.tkn.advance()
            count+=1
            self.symb_table.Define(name, type, Symb_type.ARG)
            if (self.tkn.current_token == ','):
                self.parseCurrentToken(self.verify(None))  # name
                self.tkn.advance()



        # END-
        self.endXMLNode("parameterList")
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        DBG_print("@END compileParameterList")

        return count

    def CompileClassVarDec(self):
        DBG_print("@ CompileClassVarDec")
        # START+
        start_length = len(self.working_string)
        # todo cant we have 'static int somevar[]' ?!
        self.startXMLNode("classVarDec")
        var_count=0
        # --(   identifier

        if (self.tkn.current_token == "field"):
            kind = Symb_type.FIELD
            var_count=1
        else :
            kind =Symb_type.STATIC
        self.parseCurrentToken(self.verify(None))  # static or field
        self.tkn.advance()
        type = self.tkn.current_token
        self.parseCurrentToken(self.verify(None))  # type
        self.tkn.advance()
        name = self.tkn.current_token
        self.symb_table.Define(name, type, kind)
        # --)
        self.parseCurrentToken(self.verify(TypeOfTerminal.IDENTIFIER))  # varName
        self.tkn.advance()
        # check for more varNames
        while self.tkn.current_token == ',':
            self.parseCurrentToken(self.verify(","))  # ,
            self.tkn.advance()
            # --(
            name = self.tkn.current_token
            self.symb_table.Define(name, type, kind)
            if kind==Symb_type.FIELD:
                var_count+=1
            # --)
            self.parseCurrentToken(self.verify(TypeOfTerminal.IDENTIFIER))  # varName
            self.tkn.advance()
        self.parseCurrentToken(self.verify(";"))  # ;
        self.tkn.advance()
        self.endXMLNode("classVarDec")

        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        # print(code_chunk)
        DBG_print("@END CompileClassVarDec")

        return var_count

    def CompileVarDec(self):
        DBG_print("@ CompileVarDec")
        # START+
        start_length = len(self.working_string)

        self.startXMLNode("varDec")
        count=0
        self.parseCurrentToken(self.verify("var"))  # 'var'
        self.tkn.advance()
        type = self.tkn.current_token
        self.parseCurrentToken(self.verify(None))  # type
        self.tkn.advance()
        name = self.tkn.current_token
        self.symb_table.Define(name, type, Symb_type.VAR)
        count+=1
        self.parseCurrentToken(self.verify(TypeOfTerminal.IDENTIFIER))  # varName
        self.tkn.advance()
        # check for more varNames
        while self.tkn.current_token == ',':
            self.parseCurrentToken(self.verify(","))  # ,
            self.tkn.advance()
            name = self.tkn.current_token
            self.symb_table.Define(name, type, Symb_type.VAR)
            count+=1
            self.parseCurrentToken(self.verify(TypeOfTerminal.IDENTIFIER))  # varName
            self.tkn.advance()
        self.parseCurrentToken(self.verify(";"))  # ;
        self.tkn.advance()
        self.endXMLNode("varDec")

        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        DBG_print("@END CompileVarDec")

        return count

    def compileDo(self):
        DBG_print("@ compileDo")
        # START+
        start_length = len(self.working_string)

        self.startXMLNode("doStatement")
        self.parseCurrentToken(self.verify("do"))  # 'do'
        self.tkn.advance()
        current_word = self.tkn.current_token
        self.parseCurrentToken(self.verify(None))  # subroutineName|(className|varNAme)
        self.tkn.advance()
        if self.tkn.current_token == '(':  # subroutineName(expressionList)
            self.parseCurrentToken(self.verify("("))  # (
            self.tkn.advance()
            # ^%HIDDEN_PUSH%^ 0th argument
            self.vmw.writeComment("pushing this as 0th ARG")
            self.vmw.writePush(SegmentType.POINTER,0)
            # now push other args
            num_expr=self.compileExpressionList()
            self.parseCurrentToken(self.verify(")"))  # )
            # call func
            self.vmw.writeCall(self.class_name + "." + current_word, num_expr+1)

        elif self.tkn.current_token == '.':  # className|varNAme.subroutineName(expressionList)

            self.parseCurrentToken(self.verify("."))  # .
            self.tkn.advance()

            sub_name = self.tkn.current_token
            self.parseCurrentToken(self.verify(TypeOfTerminal.IDENTIFIER))  # subroutineName
            self.tkn.advance()
            self.parseCurrentToken(self.verify("("))  # (
            self.tkn.advance()

            ident = self.symb_table.getByName(current_word)
            if not ident is None:
                # ^%HIDDEN_PUSH%^ (0th argument)
                self.vmw.writeComment("pushing the variable as 0th ARG")
                self.vmw.writePush(compileSymbToSegment(ident), ident.count)

            count = self.compileExpressionList()
            self.parseCurrentToken(self.verify(")"))  # )

            if ident is None:
                # if symbol not found then its CLASS_NAME
                self.vmw.writeCall(current_word + "." + sub_name, count)
            else:
                # then write the call
                self.vmw.writeCall(ident.type_sym + "." + sub_name, count + 1)

        # do is a procedure-call, so we pop the return value into temp:0 to discard it.
        self.vmw.writePop(SegmentType.TEMP,0)
        self.tkn.advance()
        self.parseCurrentToken(self.verify(";"))  # ;
        self.tkn.advance()

        self.endXMLNode("doStatement")

        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        DBG_print("@END compileDo")

    def compileLet(self):
        DBG_print("@ compileLet")
        # START+
        start_length = len(self.working_string)

        # self.vmw.writeComment(" START let   ")
        self.startXMLNode("letStatement")
        self.parseCurrentToken(self.verify("let"))  # 'let'
        self.tkn.advance()
        var_name = self.tkn.current_token
        self.parseCurrentToken(self.verify(TypeOfTerminal.IDENTIFIER))  # varName
        self.tkn.advance()
        is_arr=False
        if self.tkn.current_token == '[':  # if has indexing

            self.parseCurrentToken(self.verify("["))  # [
            self.tkn.advance()  # adance after [
            # GET BASE ADDR
            ident=self.symb_table.getByName(var_name)
            # we know ident cant be none!
            if ident is None:
                DBG_ERROR("Must be varName at var[] !")

            # push EXPR
            self.compileExpression()
            # Push BASE ADDR
            self.vmw.writePush(compileSymbToSegment(ident),ident.count)
            # ADD to get addr
            self.vmw.WriteArithmetic(OperType.ADD)
            # completed  A[i] --> push A+i. now save result in THAT?
            self.vmw.writePop(SegmentType.TEMP,1)
            is_arr=True
            self.parseCurrentToken(self.verify("]"))  # ']'
            self.tkn.advance()  # advance after ]

        self.parseCurrentToken(self.verify("="))  # '='
        self.tkn.advance()
        self.compileExpression()
        # result of expr is at stack bottom now
        self.parseCurrentToken(self.verify(";"))  # ';'
        # pop the result into right place
        if is_arr:
            self.vmw.writePush(SegmentType.TEMP,1)
            self.vmw.writePop(SegmentType.POINTER,1)
            # pop result of expr to array
            self.vmw.writePop(SegmentType.THAT,0)
        else:
            symbol_obj=self.symb_table.getByName(var_name)
            segment=""
            if symbol_obj.kind == Symb_type.FIELD:
                segment='this'
            else:
                segment=symbol_obj.kind
            self.vmw.writePop(segment, symbol_obj.count)
        self.tkn.advance()
        self.endXMLNode("letStatement")


        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        # self.vmw.writeComment("     let END")
        DBG_print("@END compileLet")

    def compileWhile(self):
        """
        while(cond)
            s1

        --C->

        label L1
            VM code for computing ~(cond)
            if-goto L2
            VM code for executing s1
            goto L1
        label L2

        """
        DBG_print("@ compileWhile")
        # START+
        start_length = len(self.working_string)

        # self.vmw.writeComment("** START while ***")
        self.startXMLNode("whileStatement")
        self.parseCurrentToken(self.verify("while"))  # 'while'
        self.tkn.advance()
        self.parseCurrentToken(self.verify("("))  # '('
        self.tkn.advance()
        suff=unique_suffix()
        L1="WHILE__LOOP_"+suff
        L2="WHILE__END_"+suff
        #L1
        self.vmw.WriteLabel(L1)
        # ~cond
        self.compileExpression()
        self.vmw.WriteArithmetic(OperType.NOT)
        # goto end
        self.vmw.WriteIf(L2)

        self.parseCurrentToken(self.verify(")"))  # ')'
        self.tkn.advance()
        self.parseCurrentToken(self.verify("{"))  # '{'
        self.tkn.advance()
        self.compileStatements()
        self.vmw.WriteGoto(L1)
        self.vmw.WriteLabel(L2)
        self.parseCurrentToken(self.verify("}"))  # '}'
        self.tkn.advance()
        self.endXMLNode("whileStatement")

        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        DBG_print("@END compileWhile")

    def compileReturn(self):
        DBG_print("@ compileReturn")
        # START+
        start_length = len(self.working_string)

        self.startXMLNode("returnStatement")
        self.parseCurrentToken(self.verify("return"))  # 'return'
        self.tkn.advance()
        # compile expression should maybe deal with empty exprs
        if self.tkn.current_token != ';':  # if has expression
            self.compileExpression()
        else:
            self.vmw.writePush(SegmentType.CONST,0)
        self.parseCurrentToken(self.verify(";"))  # ';'
        self.tkn.advance()  # advance after ;
        # --
        self.vmw.writeReturn()
        self.endXMLNode("returnStatement")

        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        DBG_print("@END compileReturn")

    def compileIf(self):
        """
        if (cond)
            s1
        else
            s2

        -->compile-->

          ~cond code
          if-goto L1
          s1 code
          goto L2
        label L1
          s2 code
        label L2

        :return:
        """

        DBG_print("@ compileIf")
        # START+
        start_length = len(self.working_string)

        self.startXMLNode("ifStatement")
        self.parseCurrentToken(self.verify("if"))  # 'if'

        self.tkn.advance()
        self.parseCurrentToken(self.verify("("))  # '('
        self.tkn.advance()
        suff=unique_suffix()
        self.vmw.writeComment("  if_"+suff+": ~(cond)    ")
        # compile the 'cond' expression
        self.compileExpression()
        # negate it
        self.vmw.WriteArithmetic(OperType.NOT)

        # if-goto L1

        L1_label="IF__FALSE_"+suff
        L2_label="IF__TRUE_"+suff
        self.vmw.WriteIf(L1_label)

        self.parseCurrentToken(self.verify(")"))  # ')'
        self.tkn.advance()
        self.parseCurrentToken(self.verify("{"))  # '{'
        self.tkn.advance()
        # compile s1 code
        self.compileStatements()
        # goto L2
        # label L1
        self.vmw.WriteGoto(L2_label)
        self.vmw.WriteLabel(L1_label)

        self.parseCurrentToken(self.verify("}"))  # '}'
        self.tkn.advance()
        if self.tkn.current_token == 'else':
            self.parseCurrentToken(self.verify("else"))  # 'else'
            self.tkn.advance()
            self.parseCurrentToken(self.verify("{"))  # '{'
            self.tkn.advance()
            # compile s2 code
            self.compileStatements()
            self.parseCurrentToken(self.verify("}"))  # '}'
            self.tkn.advance()
        # L2
        self.vmw.WriteLabel(L2_label)
        self.endXMLNode("ifStatement")

        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        DBG_print("@END compileIf")

    def compileStatements(self):
        DBG_print("@ compileStatements")
        # START+
        start_length = len(self.working_string)

        self.startXMLNode("statements")
        while self.tkn.current_token in ['do', 'let', 'while', 'return', 'if']:
            if self.tkn.current_token == 'do':
                self.compileDo()
            if self.tkn.current_token == 'let':
                self.compileLet()
            if self.tkn.current_token == 'while':
                self.compileWhile()
            if self.tkn.current_token == 'return':
                self.compileReturn()
            if self.tkn.current_token == 'if':
                self.compileIf()
        self.endXMLNode("statements")

        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        DBG_print("@END compileStatements")

    def compileExpressionList(self):
        DBG_print("@ compileExpressionList")
        # START+
        start_length = len(self.working_string)

        count = 0

        self.startXMLNode("expressionList")
        if self.tkn.current_token != ')':  # if we have any
            self.compileExpression()
            count = 1
            while self.tkn.current_token == ',':  # check for more expressions
                self.parseCurrentToken(self.verify(","))  # add ','
                self.tkn.advance()
                self.compileExpression()
                count += 1
        self.endXMLNode("expressionList")

        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)

        DBG_print("@END compileExpressionList")
        return count

    def compileTerm(self):
        DBG_print("@ compileTerm")
        # START+
        start_length = len(self.working_string)

        self.startXMLNode("term")
        self.parseCurrentToken(self.verify(None))  # first word
        current_word = self.tkn.current_token
        if self.tkn.current_token == '(':  # '('expression')'
            self.tkn.advance()
            self.compileExpression()  # compile expression should generate everything
            self.parseCurrentToken(self.verify(")"))  # )
            self.tkn.advance()
        elif self.tkn.current_token in ['-', '~']:  # unaryOp term
            self.tkn.advance()
            self.compileTerm()
            # we are at )
            self.vmw.WriteArithmetic(OperType.NEG if (current_word == '-') else OperType.NOT)
            # self.tkn.advance()
        elif self.tkn.tokenType() == TypeOfTerminal.INT_CONST:  # other possibilities
            self.vmw.writePush(SegmentType.CONST, current_word)
            self.tkn.advance()
        elif self.currentTokenType()==TypeOfTerminal.STRING_CONST:
            self.compile_string(self.tkn.current_token)
            self.tkn.advance()
        else:
            if self.tkn.peek_next() == '[':  # varName'['expression']'
                self.tkn.advance()  # we stand on the second word now
                self.parseCurrentToken(self.verify("["))  # [
                self.tkn.advance()
                # GET base ADDR
                var_obj=self.symb_table.getByName(current_word)
                if var_obj is None:
                    DBG_ERROR("var name must exist!")

                # push expr
                self.compileExpression()

                # push base addr
                self.vmw.writePush(compileSymbToSegment(var_obj),var_obj.count)

                # array index is at stack top

                # get final addr
                self.vmw.WriteArithmetic(OperType.ADD)
                self.vmw.writePop(SegmentType.POINTER,1)
                self.vmw.writePush(SegmentType.THAT,0)
                self.parseCurrentToken(self.verify("]"))  # ]
                self.tkn.advance()
            elif self.tkn.peek_next() == '(':  # subroutineName(expressionList)
                sub_name=self.tkn.current_token
                self.tkn.advance()  # we stand on the second word now
                self.parseCurrentToken(self.verify("("))  # (
                self.tkn.advance()

                # PUSH THIS
                self.vmw.writePush(SegmentType.POINTER,0)
                # PUSH EXPRS
                expr_count=self.compileExpressionList()

                self.parseCurrentToken(self.verify(")"))  # )

                self.vmw.writeCall(self.class_name + "." + sub_name, expr_count+1)
                self.tkn.advance()
            elif self.tkn.peek_next() == '.':  # className|varNAme.subroutineName(expressionList)
                current_word=self.tkn.current_token
                ident = self.symb_table.getByName(current_word)
                if not ident is None:
                    # ^%HIDDEN_PUSH%^ 0th argument
                    self.vmw.writeComment("pushing the variable as 0th ARG")
                    self.vmw.writePush(compileSymbToSegment(ident), ident.count)
                self.tkn.advance()  # we stand on the second word now
                self.parseCurrentToken(self.verify("."))  # .
                self.tkn.advance()
                sub_name=self.tkn.current_token
                self.parseCurrentToken(self.verify(TypeOfTerminal.IDENTIFIER))  # subroutineName
                self.tkn.advance()
                self.parseCurrentToken(self.verify("("))  # (
                self.tkn.advance()
                expr_count=self.compileExpressionList()
                self.parseCurrentToken(self.verify(")"))  # )
                if ident is None:
                    # if symbol not found then its CLASS_NAME
                    self.vmw.writeCall(current_word + "." + sub_name, expr_count)
                else:
                    # then write the call
                    self.vmw.writeCall(ident.type_sym + "." + sub_name, expr_count + 1)
                self.tkn.advance()
            elif self.tkn.tokenType() == TypeOfTerminal.IDENTIFIER:
                ident = self.symb_table.getByName(current_word)
                if not ident is None:

                    # self.vmw.writeComment("de-ref var name")
                    self.vmw.writePush(SegmentType.THIS if ident.kind == Symb_type.FIELD else ident.kind, ident.count)
                else:
                    DBG_ERROR("identifier not found")
                self.tkn.advance()
                # self.parseCurrentToken(None)  # Its unknown which symbol is next ')',';',','
            elif self.tkn.tokenType() in [TypeOfTerminal.KEYWORD, TypeOfTerminal.KWRD_CONST]:
                if current_word=='true':
                    # ^%PUSH_TRUE%^
                    self.vmw.writePush(SegmentType.CONST, 1)
                    self.vmw.WriteArithmetic(OperType.NEG)
                elif current_word in ['false','null']:
                    self.vmw.writePush(SegmentType.CONST, 0)
                elif current_word==THIS:
                    self.vmw.writePush(SegmentType.POINTER,0)
                self.tkn.advance()
                # self.parseCurrentToken(None)  # Its unknown which symbol is next ')',';',','

            else:
                DBG_ERROR("unknown Term")
        self.endXMLNode("term")
        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)

        DBG_print("@END compileTerm")

    def compileExpression(self):
        DBG_print("@ compileExpression")

        # START+
        start_length = len(self.working_string)

        # self.vmw.writeComment(" START expr:   ")

        translate = {'+': OperType.ADD,
                     '-': OperType.SUB,
                     '&': OperType.AND,
                     '|': OperType.OR,
                     '<': OperType.LT,
                     '>': OperType.GT,
                     '=': OperType.EQ}
        self.startXMLNode("expression")
        self.compileTerm()  # term
        # TODO why is '=' here?!
        while self.tkn.current_token in ['+', '-', '*', '/', '&', '|', '<', '>', '=']:  # while op
            self.parseCurrentToken(self.verify(None))  # add op
            op = self.tkn.current_token
            self.tkn.advance()
            self.compileTerm()  # term
            if op == '*':
                self.vmw.writeCall("Math.multiply", 2)
            elif op == '/':
                self.vmw.writeCall("Math.divide", 2)
            else:
                self.vmw.WriteArithmetic(translate[op])
        self.endXMLNode("expression")

        # END-
        end_length = len(self.working_string)
        code_chunk = self.working_string[start_length - 1:end_length]
        DBG_print("### code chunk ###")
        DBG_print(code_chunk)
        # self.vmw.writeComment(code_chunk)
        # self.vmw.writeComment("     expr END")
        DBG_print("@END compileTerm")


# main

def LogTHIS(msg):
    global Log
    Log+=str(msg)+'\n'

def main():
    # print(sys.argv)

    file_list = []
    input_path = os.path.abspath(sys.argv[1])  # get path
    LogTHIS("argv")
    LogTHIS(sys.argv)

    # save the folder we are in
    folder = os.path.dirname(input_path)

    LogTHIS("folder")
    LogTHIS(folder)
    LogTHIS("input")
    LogTHIS(input_path)
    LogTHIS("list folder")
    LogTHIS(os.listdir(folder))

    # save only the filename
    file_name = os.path.basename(input_path)
    # we want to compile to xml
    new_filename = file_name.split(".")[0] + ".xml"

    working_folder=""

    # if file
    if os.path.isfile(input_path):
        LogTHIS("its a filename.")
        out_path = folder + SEPARATOR + new_filename
        file_list.append(input_path)
        working_folder=folder
    # if directory

    elif os.path.isdir(input_path):
        working_folder=input_path
        LogTHIS("its a dir.")
        # list_dir= listdir(input_path)

        out_path = folder + SEPARATOR + file_name + SEPARATOR + new_filename
        file_list = [input_path + SEPARATOR + f for f in listdir(input_path) if
                     ((len(f.split("."))>1)and(f.split(".")[1] == "jack"))]


    LogTHIS("working folder")
    LogTHIS(working_folder)
    LogTHIS("list input")
    LogTHIS(os.listdir(working_folder))
    # run on each file
    for f in file_list:
        DBG_print_p11(f)
        LogTHIS("comiling: "+f)
        tkn = Tokenizer(f)
        comp = CompilationEngine(tkn)
        DBG_print(comp.printAsXML())
        if (SHOULD_OUT_XML):
            outfile = working_folder+SEPARATOR+ os.path.basename(f).split(".")[0] + ".xml"
            with open(outfile, 'w') as file:
                xml_String = str(comp.printAsXML())
                file.write(xml_String)

        if (SHOULD_OUT_VM):
            outfile_vm = working_folder+SEPARATOR+ os.path.basename(f).split(".")[0] + ".vm"
            LogTHIS("outfilevm: "+outfile_vm)
            with open(outfile_vm, 'w') as file:
                file.write(str(comp.getVMCode()))
            # DBG_print_p11(comp.getVMCode())
        # DBG_print_p11(comp.printAsXML())
        DBG_print(f + "\n")

    LogTHIS("compilation DONE!")
    LogTHIS("outputing list of files")
    LogTHIS(os.listdir(working_folder))
    LogTHIS("script end")
    # DBG_ERROR("just for print")


if __name__ == "__main__":
    main()
