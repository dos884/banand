import random


def div2(A, B):
    # assume: A,B>0.
    if (A < B):
        return 0
    sum = 0
    shift_factor = 15
    remainder = A

    while (shift_factor >= 0):
        counter = 0
        while (counter <= shift_factor):
            remainder_shifted = remainder >> (shift_factor - counter)
            if (remainder_shifted >= B):
                break
            counter += 1
        diff = (shift_factor - counter)
        sum += (1 << diff)
        remainder = remainder - (B << diff)
        shift_factor = diff
        if (remainder <= B):
            break
    return sum


def div(A, B):
    # assume: A,B>0.
    if (A < B):
        return 0
    sum = 1
    shift_factor = 1

    while (shift_factor * B < A):
        shift_factor = shift_factor << 1
    while (shift_factor != 1):
        shift_factor = shift_factor >> 1
        if ((sum + shift_factor) * B > A):
            continue
        else:
            sum += shift_factor

    return sum

def mult(A,B):
    i=0
    max_shift=16
    mask=1<<15
    while(mask & B ==0):
        max_shift-=1
        mask=mask>>1
    mask=1
    sum=0
    temp=A
    while(i<max_shift):
        if((mask & B)!=0):
            sum+=temp
            temp=temp<<1
            mask=mask<<1
        else:
            temp=temp<<1
            mask=mask<<1
        i+=1
    return sum



print(div(9,3))
print(div(18000 , 6))
print(div(32766 , 32767))

# print(mult(0,0))
# print(mult(20,30))
#
#
# # div(20,19)
# upper = 8
# lower = 2
# print("{0} : {1}, {2}/{3}".format(div(upper, lower), upper // lower, upper, lower))
# i = 0
# while (i < 20):
#     upper = random.randint(1, 100)
#     lower = random.randint(1, upper)
#     print("{0} : {1}, {2}/{3}".format(div(upper, lower), upper // lower, upper, lower))
#     i += 1
