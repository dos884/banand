load Divide.asm,
output-file Divide.out,
compare-to Divide.cmp,
output-list RAM[13]%D2.6.2 RAM[14]%D2.6.2 RAM[15]%D2.6.2;

set PC 0,
set RAM[13] 32767,   // Set test arguments
set RAM[14] 1,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 32767,   // Restore arguments in case program used them as loop counter
set RAM[14] 1,
output;
// . . .
set PC 0,
set RAM[13] 25,   // Set test arguments
set RAM[14] 14,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 25,   // Restore arguments in case program used them as loop counter
set RAM[14] 14,
output;
// . . .
set PC 0,
set RAM[13] 45,   // Set test arguments
set RAM[14] 7,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 45,   // Restore arguments in case program used them as loop counter
set RAM[14] 7,
output;
// . . .
set PC 0,
set RAM[13] 12,   // Set test arguments
set RAM[14] 3,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 12,   // Restore arguments in case program used them as loop counter
set RAM[14] 3,
output;
// . . .
set PC 0,
set RAM[13] 43,   // Set test arguments
set RAM[14] 12,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 43,   // Restore arguments in case program used them as loop counter
set RAM[14] 12,
output;
// . . .
set PC 0,
set RAM[13] 74,   // Set test arguments
set RAM[14] 3,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 74,   // Restore arguments in case program used them as loop counter
set RAM[14] 3,
output;
// . . .
set PC 0,
set RAM[13] 94,   // Set test arguments
set RAM[14] 12,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 94,   // Restore arguments in case program used them as loop counter
set RAM[14] 12,
output;
// . . .
set PC 0,
set RAM[13] 89,   // Set test arguments
set RAM[14] 50,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 89,   // Restore arguments in case program used them as loop counter
set RAM[14] 50,
output;
// . . .
set PC 0,
set RAM[13] 2,   // Set test arguments
set RAM[14] 1,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 2,   // Restore arguments in case program used them as loop counter
set RAM[14] 1,
output;
// . . .
set PC 0,
set RAM[13] 21,   // Set test arguments
set RAM[14] 16,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 21,   // Restore arguments in case program used them as loop counter
set RAM[14] 16,
output;
// . . .
set PC 0,
set RAM[13] 71,   // Set test arguments
set RAM[14] 1,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 71,   // Restore arguments in case program used them as loop counter
set RAM[14] 1,
output;
// . . .
set PC 0,
set RAM[13] 60,   // Set test arguments
set RAM[14] 42,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 60,   // Restore arguments in case program used them as loop counter
set RAM[14] 42,
output;
// . . .
set PC 0,
set RAM[13] 82,   // Set test arguments
set RAM[14] 66,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 82,   // Restore arguments in case program used them as loop counter
set RAM[14] 66,
output;
// . . .
set PC 0,
set RAM[13] 28,   // Set test arguments
set RAM[14] 1,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 28,   // Restore arguments in case program used them as loop counter
set RAM[14] 1,
output;
// . . .
set PC 0,
set RAM[13] 95,   // Set test arguments
set RAM[14] 15,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 95,   // Restore arguments in case program used them as loop counter
set RAM[14] 15,
output;
// . . .
set PC 0,
set RAM[13] 91,   // Set test arguments
set RAM[14] 33,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 91,   // Restore arguments in case program used them as loop counter
set RAM[14] 33,
output;
// . . .
set PC 0,
set RAM[13] 95,   // Set test arguments
set RAM[14] 4,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 95,   // Restore arguments in case program used them as loop counter
set RAM[14] 4,
output;
// . . .
set PC 0,
set RAM[13] 8,   // Set test arguments
set RAM[14] 3,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 8,   // Restore arguments in case program used them as loop counter
set RAM[14] 3,
output;
// . . .
set PC 0,
set RAM[13] 3,   // Set test arguments
set RAM[14] 3,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 3,   // Restore arguments in case program used them as loop counter
set RAM[14] 3,
output;
// . . .
set PC 0,
set RAM[13] 41,   // Set test arguments
set RAM[14] 30,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 41,   // Restore arguments in case program used them as loop counter
set RAM[14] 30,
output;
// . . .
set PC 0,
set RAM[13] 15,   // Set test arguments
set RAM[14] 11,
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat 6000 {
  ticktock;
}
set RAM[13] 15,   // Restore arguments in case program used them as loop counter
set RAM[14] 11,
output;
// . . .
