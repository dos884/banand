import random

START_OUT = "| RAM[13]  | RAM[14]  | RAM[15]  |"

# |       1  |       1  |      -1  |
# |      20  |       3  |      -1  |

REPEATS = 6000

script = """load Divide.asm,
output-file Divide.out,
compare-to Divide.cmp,
output-list RAM[13]%D2.6.2 RAM[14]%D2.6.2 RAM[15]%D2.6.2;

"""
out_string = START_OUT + "\n"

# Check for worst case:
upper = 32767
lower = 1
script += """set PC 0,
set RAM[13] {0},   // Set test arguments
set RAM[14] {1},
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat {2} {{
  ticktock;
}}
set RAM[13] {0},   // Restore arguments in case program used them as loop counter
set RAM[14] {1},
output;
// . . .
""".format(upper, lower, REPEATS)
out_string += "|" + "{0}".format(upper).rjust(8) + "  |" + "{0}".format(lower).rjust(8) + "  |" + "{0}".format(
        upper // lower).rjust(8) + "  |" + "\n"

# check random cases
i = 0
while (i < 20):
    upper = random.randint(1, 100)
    lower = random.randint(1, upper)
    script += """set PC 0,
set RAM[13] {0},   // Set test arguments
set RAM[14] {1},
set RAM[15] -1;  // Ensure that program initialized product to 0
repeat {2} {{
  ticktock;
}}
set RAM[13] {0},   // Restore arguments in case program used them as loop counter
set RAM[14] {1},
output;
// . . .
""".format(upper, lower, REPEATS)
    out_string += "|" + "{0}".format(upper).rjust(8) + "  |" + "{0}".format(lower).rjust(8) + "  |" + "{0}".format(
        upper // lower).rjust(8) + "  |" + "\n"
    i += 1

script_file = open("Divide.tst", 'w')
cmp_file = open("Divide.cmp", 'w')
script_file.write(script)
cmp_file.write(out_string)
script_file.close()
cmp_file.close()